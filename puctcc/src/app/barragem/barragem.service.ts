import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BarragemListagemModel, BarragemModel, IntegracaoS2id } from './models/barragem-model';

@Injectable({
  providedIn: 'root'
})
export class BarragemService {
  url: string = 'http://localhost:52957/api';

  constructor(private http: HttpClient) { }

  getAll(): Observable<BarragemListagemModel[]> {
    return this.http.get<BarragemListagemModel[]>(this.url + '/barragem');
  }

  getById(id: number): Observable<BarragemModel> {
    return this.http.get<BarragemModel>(this.url + '/barragem/' + id);
  }

  search(nome: string): Observable<BarragemListagemModel[]> {
    return this.http.get<BarragemListagemModel[]>(this.url + '/barragem/pesquisar?nome=' + nome);
  }

  getAllIntegracao(): Observable<IntegracaoS2id[]> {
    return this.http.get<IntegracaoS2id[]>(this.url + '/IntegracaoS2id');
  }
}
