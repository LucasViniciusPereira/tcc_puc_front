export enum EStatusBarragem {
    Indefinido = 0,
    Estavel = 1,
    Alerta = 2,
    Critico = 3,
    Desativada = 4,
    Rompimento = 5
}

export enum ETipoBarragem {
    Indefinido = 0,
    Montante = 1,
    Jusante = 2,
    LinhaCentro = 3,
    Desconhecido = 4
}

export enum ETipoCriticidade {
    Indefinido = 0,
    Baixo = 1,
    Medio = 2,
    Alto = 3
}

export enum ETipoMedidor {
    Indefinido = 0,
    Piezometros = 1
}