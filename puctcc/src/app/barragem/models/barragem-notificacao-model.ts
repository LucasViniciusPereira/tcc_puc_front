
export class BarragemNotificacaoModel {
    BarragemNotificacaoId: number;
    BarragemId: number;
    MedidorId: number;
    NotificacaoId: number;
    Descricao: string;
    DataCricao: string;
    ETipoCriticidade: string;
}