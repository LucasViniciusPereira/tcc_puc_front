import { BarragemNotificacaoModel } from './barragem-notificacao-model';

export class BarragemModel {
    BarragemId: number;
    Nome: string;
    Empresa: string;
    Latitude: number;
    Longitude: number;
    Ativo: boolean;
    CapacidadeTotal: number;
    CapacidadeAtual: number;
    ETipoBarragem: number;
    EStatusBarragem: number;
    BarragemNotificacaoModel: BarragemNotificacaoModel[];
}

export class BarragemListagemModel {
    BarragemId: number;
    Nome: string;
    Latitude: number;
    Longitude: number;
    EStatusBarragem: number;
}

export class IntegracaoS2id {
    Cobrade: number;
    DataDecreto: string;
    Descricao: string;
    Latitude: number;
    Longitude: number;
    Ativo: boolean;
    Municipio: string;
    Processo: string;
    Regiao: string;
    Rito: string;
    SeEcp: string;
    Tipo: string;
    Uf: string;
}