import { OnInit, OnDestroy, Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-detailsintegracao',
  templateUrl: './detailsintegracao.component.html'
})
export class DetailsIntegracaoComponent implements OnInit, OnDestroy {
  data: any;

  constructor(
    private bsModalRef: BsModalRef
  ) { }

  ngOnDestroy(): void {

  }
  ngOnInit(): void {

  }

}