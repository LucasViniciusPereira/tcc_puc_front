import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { RiscoRompimentoService } from 'src/app/shared/services/risco-rompimento.service';
import { DetailsComponent } from '../details/details.component';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  barragemId: number;

  constructor(
    private bsModalRef: BsModalRef,
    private riscoService: RiscoRompimentoService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
  }

  confirm() {
    this.riscoService.emitir(this.barragemId).subscribe(id => {
      this.riscoService.buscarRiscoRompimento(id).subscribe(data => {
        this.messageService.sendMessage(data);
        this.bsModalRef.hide();
      });
    })
  }
}
