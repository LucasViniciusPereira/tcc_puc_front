import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BarragemModel } from '../models/barragem-model';
import { BarragemService } from '../barragem.service';
import { EStatusBarragem, ETipoBarragem, ETipoMedidor, ETipoCriticidade } from '../Enuns';
import { AlertComponent } from '../alert/alert.component';
import { MessageService } from '../message.service';
import { Subscription } from 'rxjs';
import { RiscoRompimentoService } from 'src/app/shared/services/risco-rompimento.service';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnDestroy {
  model: BarragemModel;
  modalAlert: BsModalRef;
  data: any;
  subscription: Subscription;

  constructor(
    private bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private messageService: MessageService,
    private riscoService: RiscoRompimentoService
  ) {

    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.atribuirRiscoRompimento(message);
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openModalAlert(template: TemplateRef<any>) {
    const initialState = { barragemId: this.data.barragemId, barragem: this.data.nome };
    this.modalAlert = this.modalService.show(AlertComponent,
      Object.assign(
        { initialState },
        { class: 'modal-sm' }
      ));
  }

  get showRiscoRompimento() {
    return this.data.riscoRompimento &&
      this.data.riscoRompimento.ativo;
  }

  get showRiscoRompimentoBotoes() {
    return this.data.riscoRompimento &&
      this.data.riscoRompimento.rompimento &&
      this.data.riscoRompimento.ativo;
  }

  atribuirRiscoRompimento(data: any) {
    this.data.riscoRompimento = data.text;
  }

  atribuirBarragem(data: any) {
    this.data = data;
  }

  confirmarRompimento() {
    this.riscoService.confirmar(this.data.riscoRompimento.riscoRompimentoId).subscribe(d => {
      this.atribuirBarragem(d);
    });
  };
  cancelarRompimento() {
    this.riscoService.cancelar(this.data.riscoRompimento.riscoRompimentoId).subscribe(d => {
      this.atribuirBarragem(d);
    });
  };

  getEnumStatus(status) {
    return EStatusBarragem[status];
  }
  getEnumTipoBarragem(tipo) {
    return ETipoBarragem[tipo];
  }
  getEnumTipoMedidor(tipo) {
    return ETipoMedidor[tipo];
  }
  getEnumCriticidade(tipo) {
    return ETipoCriticidade[tipo];
  }
  getEnumCriticidadeString(tipo) {
    return ETipoCriticidade[tipo].toString();
  }
}
