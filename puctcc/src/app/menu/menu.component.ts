import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, switchMap, map, filter } from 'rxjs/operators';

import { BarragemService } from '../barragem/barragem.service';
import { BarragemListagemModel } from '../barragem/models/barragem-model';
import { MapsComponent } from '../maps/maps.component';
import { AuthenticationService } from '../_helpers/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  private _opened: boolean = false;
  nomeSearch = new FormControl;
  selected: string;
  autocomplete$: BarragemListagemModel[] = [];
  private _toggleOpened(): void {
    this._opened = !this._opened;
  }
  constructor(
    private barragemService: BarragemService,
    private mapsComponent: MapsComponent,
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.nomeSearch.valueChanges.pipe(
      filter(val => val.length > 2),
      debounceTime(300),
      switchMap(value => this.barragemService.search(value)),
    )
      .subscribe(data => {
        this.autocomplete$ = data || [];
      });
  }

  onSelect(event) {
    this.mapsComponent.selectMarker(event.item.barragemId, null);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
