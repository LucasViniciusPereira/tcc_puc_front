import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_helpers/authGuard';

const routes: Routes = [
  { path: '', redirectTo: 'maps', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'maps', component: IndexComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
