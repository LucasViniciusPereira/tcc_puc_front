import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, finalize } from 'rxjs/operators';
import { SpinnerService } from './services/spinner.service';

export class HttpErrorInterceptor implements HttpInterceptor {
    private requests: HttpRequest<any>[] = [];

    constructor(private loaderService: SpinnerService) { }

    removeRequest(req: HttpRequest<any>) {
        const i = this.requests.indexOf(req);
        if (i >= 0) {
            this.requests.splice(i, 1);
        }
        this.loaderService.isLoading.next(this.requests.length > 0);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.requests.push(request);
        this.loaderService.isLoading.next(true);

        return next.handle(request)

            .pipe(

                catchError((error: HttpErrorResponse) => {
                    if (error.status === 401) {
                        console.log('Sem autorização');
                        //location.reload(true);
                    }
                    this.removeRequest(request);
                    let errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        errorMessage = `Error: ${error.error.message}`;
                    } else {
                        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
                    }
                    //window.alert(errorMessage);
                    return throwError(errorMessage);
                }),
                finalize(() => this.removeRequest(request))
            )
    }
}