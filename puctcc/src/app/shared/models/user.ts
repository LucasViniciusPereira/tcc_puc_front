export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    nameUser: string;
    accessToken?: string;
}
