import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class SpinnerService {
    public isLoading = new BehaviorSubject(false);
    constructor() { }
}