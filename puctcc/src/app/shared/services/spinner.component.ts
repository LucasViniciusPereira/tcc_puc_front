import { OnInit, Component } from '@angular/core';
import { SpinnerService } from './spinner.service';

@Component({
    selector: 'app-spinner',
    template: '<div class="overlay" [hidden]="!loading"><div class="progress-loader" >Carregando...</div></div>'
  })
  export class SpinnerComponent implements OnInit{
    loading: boolean;
    constructor(private loaderService: SpinnerService) {
        this.loaderService.isLoading.subscribe((v) => {
          console.log(v);
          this.loading = v;
        });
      }

    ngOnInit(){ }
  }