import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RiscoRompimentoService {
  url: string = 'http://localhost:52957/api';

  constructor(private http: HttpClient) { }

  emitir(barragemId: number): Observable<any> {
    return this.http.post<any>(this.url + '/RiscoRompimento/emitirRiscoRompimento?barragemId=' + barragemId, null);
  }

  confirmar(id: number): Observable<any> {
    return this.http.post<any>(this.url + '/RiscoRompimento/confirmarRompimento?id=' + id, null);
  }

  cancelar(id: number): Observable<any> {
    return this.http.post<any>(this.url + '/RiscoRompimento/cancelarRiscoRompimento?id=' + id, null);
  }

  buscarRiscoRompimento(id: number): Observable<any> {
    return this.http.get<any>(this.url + '/RiscoRompimento/' + id);
  }
}
