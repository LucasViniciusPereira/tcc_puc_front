import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { MenuComponent } from './menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from 'ng-sidebar';
import { MapsComponent } from './maps/maps.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DetailsComponent } from './barragem/details/details.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './shared/HttpErrorInterceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TabsModule, BsDropdownModule } from 'ngx-bootstrap';
import { LoginComponent } from './login/login.component';
import { JwtInterceptor } from './shared/JwtInterceptor';
import { AlertComponent } from './barragem/alert/alert.component';
import { DetailsIntegracaoComponent } from './barragem/detailsIntegracao/detailsIntegracao.component';
import { SpinnerComponent } from './shared/services/spinner.component';

@NgModule({
   declarations: [
      AppComponent,
      IndexComponent,
      MenuComponent,
      MapsComponent,
      DetailsComponent,
      DetailsIntegracaoComponent,
      LoginComponent,
      AlertComponent,
      SpinnerComponent,
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      TypeaheadModule.forRoot(),
      SidebarModule.forRoot(),
      ModalModule.forRoot(),
      TabsModule.forRoot(),
      BsDropdownModule.forRoot(),
      AgmCoreModule.forRoot({ apiKey: 'AIzaSyCyjYrseq-uoNsogQSKMLZEZdCgvQbOXMg' }),
      AgmJsMarkerClustererModule
   ],
   entryComponents: [
      DetailsComponent,
      DetailsIntegracaoComponent,
      AlertComponent
   ],
   providers: [
      GoogleMapsAPIWrapper,
      { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
