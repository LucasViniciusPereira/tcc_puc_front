import { Component, OnInit, TemplateRef, Injectable, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DetailsComponent } from '../barragem/details/details.component';
import { BarragemListagemModel, IntegracaoS2id } from '../barragem/models/barragem-model';
import { BarragemService } from '../barragem/barragem.service';
import { MouseEvent, GoogleMapsAPIWrapper, MarkerManager } from '@agm/core'
import { DetailsIntegracaoComponent } from '../barragem/detailsIntegracao/detailsIntegracao.component';
declare var google: any;

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  latitude: number = -50.2360099;
  longitude: number = -46.8435756;
  mapType: string = 'roadmap';
  zoom: number = 4;
  minZoom: number = 3;
  maxZoom: number = 15;
  selectedMarker;
  modalBarragemDetails: BsModalRef;
  modalBarragemDetailsIntegracao: BsModalRef;
  markers: BarragemListagemModel[];
  markersIntegracao: IntegracaoS2id[];

  constructor(
    private modalService: BsModalService,
    private barragemService: BarragemService,
  ) {
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.barragemService.getAll().subscribe(data => {
      this.markers = data;
    });
    this.barragemService.getAllIntegracao().subscribe(data => {
      this.markersIntegracao = data;
    });
  }

  getIcon(eStatusBarragem) {
    let url = '';
    if (eStatusBarragem === 1) {
      url = 'assets/icons/green.png';
    }
    else if (eStatusBarragem === 2) {
      url = 'assets/icons/laranja.png';
    }
    else if (eStatusBarragem === 3) {
      url = 'assets/icons/red.png';
    }
    else if (eStatusBarragem === 4) {
      url = 'assets/icons/blue.png';
    }
    else {
      url = 'assets/icons/gray.png';
    }
    return url;
  }

  addMarker(lat: number, lng: number) {
    //console.log('lat: ' + lat + ", lng: " + lng);
  }

  selectMarker(barragemId: number, template: TemplateRef<any>) {
    this.barragemService.getById(barragemId).subscribe(data => {
      const initialState = { data };
      this.modalBarragemDetails = this.modalService.show(DetailsComponent,
        Object.assign(
          { initialState },
          { class: 'gray modal-lg' }
        ));
    });
  }
  selectMarkerIntegracao(data: any, template: TemplateRef<any>) {
    const initialState = { data };
    this.modalBarragemDetailsIntegracao = this.modalService.show(DetailsIntegracaoComponent,
      Object.assign(
        { initialState },
        { class: 'gray modal-lg' }
      ));
  }
}
